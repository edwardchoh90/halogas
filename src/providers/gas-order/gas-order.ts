import 'rxjs/add/operator/map';
import { gasOrderObject } from "../../interface/GasOrderObject"
/*
  Generated class for the GasOrderProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

export class GasOrderProvider {

  gasOrder:gasOrderObject = {};


  constructor() {
    console.log('Hello GasOrderProvider Provider');
  }


  //resets the order detail;
   newOrder(transaction){
    this.gasOrder = {};
    this.gasOrder.transactionType = transaction;
  }

  getCurrentOrder(){
     return this.gasOrder;
  }

  setGasBrand( brand){
    this.gasOrder.gasBrand = brand;
  }

  setGasSize (size){
    this.gasOrder.gasSize = size;
  }

  setGasQuantity (qty){
    this.gasOrder.quantity = qty;
  }

  setPreferredTime(time){
    this.gasOrder.preferredTime = time;
  }


  setAdditional(note){
    this.gasOrder.additionalNote = note;
  }

  // Delivery Related Function

  setDeliveryNote(note){
    this.gasOrder.deliveryNote = note;
  }

  setDeliveryAddress(address){
    this.gasOrder.deliveryAddress = address;
  }

  setDeliveryTime(time){
    this.gasOrder.deliveryTime = time;
  }




}
