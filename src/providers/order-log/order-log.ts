import 'rxjs/add/operator/map';
import { gasOrderObject } from "../../interface/GasOrderObject"

/*
  Generated class for the OrderLogProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

export class OrderLogProvider {
  orderList:Array<gasOrderObject> = [];
  constructor() {
    console.log('Hello OrderLogProvider Provider');
  }

  addOrder(order:gasOrderObject){
    this.orderList.push(order);
  }

  getOrder(){

    return this.orderList;
  }

}
