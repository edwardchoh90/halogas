import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UserProvider {

  profilePic;
  userName;
  phoneNumber;
  email;

  constructor(public http: Http) {
    console.log('Hello UserProvider Provider');
    this.profilePic = "assets/img/blankAvatar.png";
    this.userName ="";
    this.phoneNumber ="";
    this.email="";
  }

  loadUserName(val){
    this.userName = val;
  }

  loadProfilePic(val){
    this.profilePic= val;
  }

  loadEmail(val){
    this.email = val;
  }

  loadPhoneNumber(val){
    this.phoneNumber = val;
  }

}
