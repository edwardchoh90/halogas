import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { OrderPage } from '../pages/order/order'
import { ProfilePage } from '../pages/profile/profile'
import { SignupPage } from '../pages/signup/signup';
import { SmSverificationPage } from "../pages/sm-sverification/sm-sverification";
import { ProfileCreatorPage } from '../pages/profile-creator/profile-creator';
import { LoginPage } from "../pages/login/login";
import { GasSelectionPage } from "../pages/gas-selection/gas-selection"
import { SizeQtySelectPage } from "../pages/size-qty-select/size-qty-select"
import { DeliveryDetailsPage } from "../pages/delivery-details/delivery-details"
import { OrderSummaryPage } from "../pages/order-summary/order-summary"

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { DatePicker } from '@ionic-native/date-picker'


import { UserProvider } from '../providers/user/user';
import { GasOrderProvider } from '../providers/gas-order/gas-order';
import { OrderLogProvider } from '../providers/order-log/order-log';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    SignupPage,
    SmSverificationPage,
    ProfileCreatorPage,
    OrderPage,
    ProfilePage,
    LoginPage,
    GasSelectionPage,
    SizeQtySelectPage,
    DeliveryDetailsPage,
    OrderSummaryPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    SignupPage,
    SmSverificationPage,
    ProfileCreatorPage,
    OrderPage,
    ProfilePage,
    LoginPage,
    GasSelectionPage,
    SizeQtySelectPage,
    DeliveryDetailsPage,
    OrderSummaryPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    FileTransfer,
    FilePath,
    Camera,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    GasOrderProvider,
    DatePicker,
    OrderLogProvider,
  ]
})
export class AppModule {}
