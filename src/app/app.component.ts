import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup'
import { OrderPage } from '../pages/order/order'
import { ProfilePage } from '../pages/profile/profile'
import { UserProvider } from '../providers/user/user'
import { SizeQtySelectPage } from '../pages/size-qty-select/size-qty-select'


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = SignupPage;
  //rootPage: SizeQtySelectPage;
  profilePic;
  userName;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
              public events: Events, public userService:UserProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Order', component: OrderPage },
      { title: 'Profile', component: ProfilePage}
    ];

    this.events.subscribe("intoHomePage", (info)=>{
      this.profilePic = this.userService.profilePic;
      this.userName = this.userService.userName;

    })

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
