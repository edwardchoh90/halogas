/**
 * Created by jkchoh-nb on 8/2/2017.
 */

export interface gasOrderObject{

  transactionType?:string;
  gasBrand?:string;
  gasSize?:string;
  quantity?:number;
  preferredTime?:string;
  additionalNote?:string;
  deliveryNote?:string;
  deliveryTime?:Date;
  deliveryAddress?:string


}
