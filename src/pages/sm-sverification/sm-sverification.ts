import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { ProfileCreatorPage } from '../profile-creator/profile-creator';

/**
 * Generated class for the SmSverificationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-sm-sverification',
  templateUrl: 'sm-sverification.html',
})
export class SmSverificationPage {
  verificationCode;
  userAttempt:String;

  constructor(public navCtrl: NavController, public navParams: NavParams, public toast:
    ToastController, public loadingCtrl: LoadingController) {
    this.verificationCode = "1234";

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SmSverificationPage');
  }

  pushToHome(){
    if (this.userAttempt!=this.verificationCode){

        let invalidMsg = this.toast.create({
          message: 'Incorrect code.Please try again',
          duration: 1500,
          position: 'bottom'
        });

        invalidMsg.present();
    } else {

      let loading = this.loadingCtrl.create({
        duration: 300,
        content: 'Verifying'
      });
      loading.present();
        this.navCtrl.push(ProfileCreatorPage);
    }
  }

  backtoSignUp(){
    this.navCtrl.pop()
  }

}
