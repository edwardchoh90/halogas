import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { GasSelectionPage } from '../../pages/gas-selection/gas-selection';
import { GasOrderProvider } from '../../providers/gas-order/gas-order'
import { OrderLogProvider } from "../../providers/order-log/order-log"
import { OrderSummaryPage } from "../../pages/order-summary/order-summary"
/**
 * Generated class for the OrderPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {
showguide;
orderHist= [];
transactionType ="";



  constructor(public navCtrl: NavController, public navParams: NavParams, public altCtrl: AlertController,
              public gasOrderService:GasOrderProvider, public orderLog: OrderLogProvider) {
    this.showguide = false;
  }

  ionViewWillLoad() {
    this.orderHist = this.orderLog.getOrder();
    // show the big new Order Thingy
    if(this.orderHist.length ==0){
      this.showguide =true;
    }
  }

  newOrder(){
    let newOrderAlert = this.altCtrl.create({
      title: 'New Order',
      message: 'Which order do you want to choose?',
      buttons: [
        {
          text: 'New Order',
          handler: () => {
            this.transactionType = "New Order";
            this.gasOrderService.newOrder(this.transactionType);
            this.navCtrl.push(GasSelectionPage,{transaction:this.transactionType})
          }
        },
        {
          text: 'Refill',
          handler: () => {
            this.transactionType = "Refill";
            this.gasOrderService.newOrder(this.transactionType);
            this.navCtrl.push(GasSelectionPage,{transaction:this.transactionType})
          }
        }
      ]
    });
    newOrderAlert.present();
  }

  viewEntry(index){
    this.navCtrl.push(OrderSummaryPage,{listIndex:index})
  }



}
