import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { GasOrderProvider } from '../../providers/gas-order/gas-order'
import { gasOrderObject } from "../../interface/GasOrderObject"
import { OrderPage } from "../../pages/order/order"
import { OrderLogProvider } from "../../providers/order-log/order-log"


/**
 * Generated class for the OrderSummaryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-order-summary',
  templateUrl: 'order-summary.html',
})
export class OrderSummaryPage {

  gasOrder:gasOrderObject;
  indexFromMain:number;
  fromSummary= false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public altCtrl:AlertController,
              public gasOrderService:GasOrderProvider, public orderLogService:OrderLogProvider) {
  }

  ionViewWillLoad() {
    console.log('ionViewDidLoad OrderSummaryPage');

    this.indexFromMain = this.navParams.get("listIndex")

    if (this.indexFromMain !=null){
      this.gasOrder = this.orderLogService.getOrder()[this.indexFromMain];
      this.fromSummary = true
    }else {
      this.gasOrder = this.gasOrderService.getCurrentOrder();
    }
  }

  confirmOrder(){
    console.log(this.gasOrder);
    let confirmAlert = this.altCtrl.create({
              title: " Confirm Order",
              subTitle: " Your order will be confirmed.",
              buttons: [
               {
                 text: 'Cancel',
                 role: 'cancel',
                 handler: () => {
                   console.log('Cancel clicked');
                 }
                },
              {
                text: 'confirm',
                handler: () => {
                this.orderLogService.addOrder(this.gasOrder);
                  this.navCtrl.setRoot(OrderPage)
                }
              }
                      ]
    });

    confirmAlert.present();

  }

}
