import { Component } from '@angular/core';
import { NavController, NavParams, ToastController,LoadingController, Events} from 'ionic-angular';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { OrderPage } from "../../pages/order/order";
import { UserProvider } from "../../providers/user/user"


/**
 * Generated class for the ProfileCreatorPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-profile-creator',
  templateUrl: 'profile-creator.html',
})
export class ProfileCreatorPage {

  //code Upload taken from https://devdactic.com/ionic-2-images/
  imageSrc;
  picStatus;
  name;
  email;
  profileForm:FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private file: File,
              private camera: Camera, public toast: ToastController,
              public formbuilder: FormBuilder,public userService:UserProvider, public loadingCtrl:LoadingController,
              public events: Events) {

    this.imageSrc = "assets/img/newphoto.png";

    this.profileForm = formbuilder.group({
      name:['',Validators.compose([Validators.required, Validators.minLength(9),Validators.maxLength(10),Validators.pattern('[0-9 ]*')])],
      email:['',Validators.email]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileCreatorPage');

    //remove SMS verification
    this.navCtrl.remove(1);
  }



  private uploadPhoto(): void {
    let cameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      quality: 100,
      targetWidth: 1000,
      targetHeight: 1000,
      encodingType: this.camera.EncodingType.JPEG,
      correctOrientation: true
    }

    this.camera.getPicture(cameraOptions)
      .then(file_uri => {
        this.imageSrc = file_uri;
      },err => {
        let invalidMsg = this.toast.create({
          message: err,
          duration: 1500,
          position: 'bottom'
        });
        invalidMsg.present();
      });
  }

  homePage(){

    var name = this.removeWhiteSpace(this.name);
    var email = this.removeWhiteSpace(this.email);

    if (name!=null){

        //load profile Image to Provider
      if (this.imageSrc != "assets/img/newphoto.png" ){
        this.userService.loadProfilePic(this.imageSrc);
      }


      //load Email to Provider
      if (email != null) {
        this.userService.loadEmail(this.email);
      }

      this.userService.loadUserName(this.name);

      this.events.publish("intoHomePage");

      let loading = this.loadingCtrl.create({
        duration: 300,
        content: 'Setting Up'
      });

      loading.present();

      this.navCtrl.setRoot(OrderPage);

    } else {

      let invalidMsg = this.toast.create({
        message: " Please key in name",
        duration: 1500,
        position: 'bottom'
      });
      invalidMsg.present();

    }






  }

  removeWhiteSpace(str){

    if (str!=null) {

      str = str.replace(/^\s+/g, "");
      if (str.length == 0) {
        return null;
      }

      else {
        return str
      }
    }
  }

}
