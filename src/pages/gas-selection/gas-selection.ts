import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import {SizeQtySelectPage} from  '../../pages/size-qty-select/size-qty-select'
import { GasOrderProvider } from '../../providers/gas-order/gas-order'

/**
 * Generated class for the GasSelectionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-gas-selection',
  templateUrl: 'gas-selection.html',
})
export class GasSelectionPage {

  //initial object to select

  petronas ={
  name : "petronas",
  sizeSelection:[
    {size:14,selected:false},
    {size:16,selected:false},
    {size:20,selected:false}
    ],
  selected:false,
  };

  petron ={
    name : "petron",
    sizeSelection:[
      {size:12,selected:false},
      {size:18,selected:false}
    ],
    selected:false,
  };

  solarGas ={
    name : "solarGas",
    sizeSelection:[
      {size:14,selected:false},
      {size:20,selected:false}
    ],
    selected:false,
  };

  miraGas ={
    name : "miraGas",
    sizeSelection:[
      {size:12,selected:false},
      {size:14,selected:false},
      {size:20,selected:false}
    ],
    selected:false,
  };
  //product catalog
  selections = [this.petronas,this.petron,this.miraGas,this.solarGas];
  //available size Choices
  sizeSelections = [];
  //Keep track of State
  hideSize = true;
  gasSelected = null;
  priceSelected = null;
  gasSizeSelected="";

  constructor(public navCtrl: NavController, public navParams: NavParams, public toast: ToastController, public gasService:GasOrderProvider) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GasSelectionPage');
  }


  gasSelect(gasName){

    this.resetAll();
    this.hideSize = false;
    for (var i in this.selections){
        if (gasName ==(this.selections[i].name)){
          this.selections[i].selected = true;
          this.sizeSelections = this.selections[i].sizeSelection;
          this.gasSelected = this.selections[i].name;
          break;
        }
    }
  }

  sizeSelect(GasSize,index){
    console.log(index);
    this.priceSelected = true;
    this.resetPrice(); //clean previous choice;
    this.sizeSelections[index].selected = true;
    this.gasSizeSelected = GasSize.size;
    console.log("Gas Size Selected",this.gasSizeSelected);
  }

  qtySizeSelect(){
    if (this.priceSelected != true){
    let warning = this.toast.create({
      message: 'Please select the cooking gas and size',
      duration: 1500,
      position: 'bottom'
    });
    warning.present();
    } else {
      this.gasService.setGasBrand(this.gasSelected);
      this.gasService.setGasSize(this.gasSizeSelected);
      this.navCtrl.push(SizeQtySelectPage);
    }


  }


  private resetAll(){
    for (var i in this.selections){
      this.selections[i].selected = false;
    }
    this.resetPrice();
    this.priceSelected = null;
    this.gasSelected = "";
  }

  private resetPrice(){
    for (var i in this.sizeSelections){
      this.sizeSelections[i].selected = false;
    }
  }

}
