import { Component } from '@angular/core';
import { NavController, NavParams, ToastController,AlertController } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';
import { DatePipe } from '@angular/common';
import { GasOrderProvider } from '../../providers/gas-order/gas-order'
import { OrderSummaryPage} from "../../pages/order-summary/order-summary"

/**
 * Generated class for the DeliveryDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-delivery-details',
  templateUrl: 'delivery-details.html',
})
export class DeliveryDetailsPage {
  dateTime = new Date;
  address="";
  deliveryNotes;

  constructor(public navCtrl: NavController, public navParams: NavParams, private datePicker: DatePicker, private toast: ToastController,
              private altCtrl: AlertController, private gasOrderService:GasOrderProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeliveryDetailsPage');
  }

  pickDateTime(){

    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    }).then(
      date => {
        this.dateTime = date;
      },
      err => {
        this.dateTime = err;
      }
    );

  }

  pushToOrderSummary(){

    console.log("this address",this.address);
    console.log("this address length",this.address.length);
    if (this.address !=null && this.address.length > 0) {
      this.gasOrderService.setDeliveryNote(this.deliveryNotes);
      this.gasOrderService.setDeliveryTime(this.dateTime);
      this.gasOrderService.setDeliveryAddress(this.address);
      this.navCtrl.push(OrderSummaryPage);
    } else {

    let warning = this.toast.create({
      duration:1800,
      message:"Please key in your address",
      position: "bottom"
    })
    warning.present();
    }
  }

}
