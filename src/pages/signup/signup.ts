import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { SmSverificationPage } from "../../pages/sm-sverification/sm-sverification"
import { UserProvider } from "../../providers/user/user"

/**
 * Generated class for the SignupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  signUpForm: FormGroup;
  phoneNumber: String;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formbuilder: FormBuilder,
              public toast: ToastController, public userService:UserProvider) {


    this.signUpForm = formbuilder.group({
      phoneNumber:['',Validators.compose([Validators.required, Validators.minLength(9),Validators.maxLength(10),Validators.pattern('[0-9 ]*')])],
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  pushToVerification(){

    var inputPhoneNUmber:String = this.removeWhiteSpace(this.phoneNumber);


 if (inputPhoneNUmber==null) {

      let invalidMsg = this.toast.create({
        message: 'Please key in your phone number',
        duration: 1500,
        position: 'bottom'
      });
      invalidMsg.present();

    } else {
      this.userService.loadPhoneNumber(this.phoneNumber); // load phone Number Information
      this.navCtrl.push(SmSverificationPage);
    }
  }

  removeWhiteSpace(str){

    if (str!=null) {

      str = str.replace(/^\s+/g, "");
      if (str.length == 0) {
        return null;
      }

      else {
        return str
      }
    }
  }

}
