import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { DeliveryDetailsPage } from "../../pages/delivery-details/delivery-details"
import { GasOrderProvider } from '../../providers/gas-order/gas-order'

/**
 * Generated class for the SizeQtySelectPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-size-qty-select',
  templateUrl: 'size-qty-select.html',
})
export class SizeQtySelectPage {
  quantity;
  preferredTime;
  deliveryNote;

  constructor(public navCtrl: NavController, public navParams: NavParams, public gasOrderService:GasOrderProvider,public toast:ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SizeQtySelectPage');
  }

  goToDeliveryDetails(){

    if (this.quantity == null){
    let warningQty = this.toast.create({
      duration:1500,
      message: "Please select Quantity",
      position: "bottom",
    })

      warningQty.present();
    } else if (this.preferredTime == null){
      let warningTime = this.toast.create({
        duration:1500,
        message: "Please select your preferred time",
        position: "bottom",
      })

      warningTime.present();
    } else {
      this.gasOrderService.setDeliveryNote(this.deliveryNote);
      this.gasOrderService.setPreferredTime(this.preferredTime);
      this.gasOrderService.setGasQuantity(this.quantity);
      this.navCtrl.push(DeliveryDetailsPage);
    }
  }

}
